/* window.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either verutilon 3 of the License, or
 * (at your option) any later verutilon.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace Jellybean {    // Widget documentation
    [GtkTemplate (ui = "/garden/turtle/Jellybean/main.ui")]         //
    public class Window : Adw.ApplicationWindow {                   // General
        [GtkChild] private unowned Adw.ToastOverlay to;             // Toast overlay for application
        [GtkChild] private unowned Adw.NavigationView nv;           // Navigation view for application
        [GtkChild] private unowned Adw.NavigationPage iv;           // Item view navigation page. (For editing properties)
        [GtkChild] private unowned Adw.NavigationPage ev;           // Edit view navigation page. (For editing properties)
  //
  // Main View
        [GtkChild] private unowned Adw.Clamp mv_adwc;               // Main view clamp (for showing No Results status page)
        [GtkChild] private unowned Adw.StatusPage mv_status_page;   // No Results status page
        [GtkChild] public  unowned Gtk.ListBox mv_lb;               // The main list box for showing items
        [GtkChild] private unowned Gtk.SearchEntry search_entry;    // Search bar
        [GtkChild] private unowned Gtk.SearchBar search_bar;        // Search bar parent, that hides and shows it
        [GtkChild] private unowned Gtk.MenuButton mv_menu;          // Main Menu button
  //
  // Item View
        [GtkChild] private unowned Gtk.Image status_icon;           // Displays status page icon
        [GtkChild] private unowned Gtk.Label title_label;           // Title of status page
        [GtkChild] private unowned Gtk.Label description_label;     // Description of status page
        [GtkChild] private unowned Gtk.Button sp1_sp_drain;         // “Use…” button (for controlling sensitivity)
  //
  // Edit View
        [GtkChild] private unowned Adw.EntryRow ev_title;           // Title                    (Edit mode)
        [GtkChild] private unowned Gtk.MenuButton ev_icon_button;   // Icon selector button     (Edit mode)
        [GtkChild] private unowned Gtk.Adjustment nmadjustment;     // Current number           (Edit mode)
        [GtkChild] private unowned Adw.EntryRow ev_unit;            // Unit                     (Edit mode)
        [GtkChild] private unowned Gtk.Adjustment quadjustment;     // Standard usage amount    (Edit mode)
        [GtkChild] private unowned Gtk.Adjustment lsadjustment;     // Low stock amount         (Edit mode)
        [GtkChild] private unowned Gtk.Button ev_db;                // Delete button            (Edit mode)
        [GtkChild] private unowned Gtk.Box popover_box;             // Box for containing icon buttons shown on the icon selector button


        // Function that runs backend management of jellybeans
        private Jellybean.JellybeanUtil util = new Jellybean.JellybeanUtil ();
        // Int to store the current jellybean's index
        private int jellybean_index;
        // Marks whether the edit window is closed while being saved
        private bool is_save = false;
        // Marks whether the edit window is creating a new jellybean
        private bool is_new_jellybean = false;
        // Status of item usage, can be used to block toast popups on an undo action
        uint8 use_status = 0;
        // Whether the edit view is actually shown: used to inhibit toast on forward swipe
        bool is_ev_shown = false;
        // Search terms for jellybean icons
        private Jellybean.StringSearchTerms string_search_terms = new Jellybean.StringSearchTerms ();
        // Whether the search function returns results
        private bool is_showing_results = false;


        // Regex to filter rows
        private Regex regex { get; set; }
        // Application accessor method
        public Gtk.Application app { get; construct; }

        // Constructor with app
        public Window (Gtk.Application app) {
            Object (
                    application: app,
                    app: app
            );
        }

        construct {
            // Define actions
            ActionEntry[] action_entries = {
                { "discard-dialog", () => { // Activated when Cancel button is pressed
                    if (nv.get_visible_page ().tag != "ev") return;
                    nv.pop_to_tag ("iv");
                }},
                { "find", () => { // Action on Ctrl + F to launch search bar
                    if (nv.get_visible_page ().tag != "mv") return;
                    search_bar.search_mode_enabled = true;
                }},
                { "save-jellybean", this.save_jellybean },
                { "use-jellybean", this.use_jellybean },
                { "refill-jellybean", this.refill_jellybean },
                { "add-jellybean", this.add_jellybean },
                { "edit-jellybean", () => { this.edit_jellybean (); } }, // Call without arguments— thus, must be wrapped in lambda
                { "delete-jellybean", this.delete_jellybean },
                { "main-menu", mv_menu.activate_default },
            };
            this.add_action_entries (action_entries, this); // Add actions

            // Since gettext does not work without an init outside a window, we will do this here.
            ICON_TOOLTIPS = {
                _("Ticket"), _("Paper"), _("Bread"), _("Cafe"), _("Medicine"), _("Wallet")
            };

            // Set accelerators
            app.set_accels_for_action ("win.add-jellybean", { "<Primary>n" });
            app.set_accels_for_action ("win.discard-dialog", { "<Primary>l" });
            app.set_accels_for_action ("win.save-jellybean", { "<Primary>s" });
            app.set_accels_for_action ("win.use-jellybean", { "<Primary>u" });
            app.set_accels_for_action ("win.refill-jellybean", { "<Primary>r" });
            app.set_accels_for_action ("win.edit-jellybean", { "<Primary>e" });
            app.set_accels_for_action ("win.delete-jellybean", { "<Primary>d" });
            app.set_accels_for_action ("win.main-menu", { "F10" });
            app.set_accels_for_action ("win.preferences", { "<Primary>comma" });
            app.set_accels_for_action ("win.show-help-overlay", { "<Primary>question" });
            app.set_accels_for_action ("win.find", { "<Primary>f" });

            Jellybean.show_func = this.show_jellybean;                      // Add callback function
            util.update_rows (mv_lb);                                       // Run initial row creation
            ev.hidden.connect (discard_dialog);                             // Bind discard action to leaving edit view
            ev.shown.connect (() => { is_ev_shown = true; });               // Set edit view to shown
            populate_icon_popover ();                                       // Populate the icon popover

            // Get the next page for forward swipes
            nv.get_next_page.connect(() => {
                switch (nv.get_visible_page ().tag) {
                    case "iv":
                        edit_jellybean (false);
                        is_ev_shown = false;
                        return ev;
                    case "ev":
                    case "mv":
                    default:
                        return null;
                }
            });

            // Bind filtering of main list box
            mv_lb.set_filter_func (filter_row);
            search_entry.search_changed.connect (() => {
                if (!is_showing_results) mv_adwc.set_child (mv_lb);
                is_showing_results = false;
                try {
                    regex = new Regex (Regex.escape_string (search_entry.text), CASELESS, PARTIAL_SOFT);
                } catch (Error e) {
                    critical (e.message);
                    return;
                }
                mv_lb.invalidate_filter ();
                if (!is_showing_results && search_bar.get_search_mode ()) mv_adwc.set_child (mv_status_page);
            });

            // Load help overlay`
            Gtk.Builder ui_builder = new Gtk.Builder ();
            try {
                ui_builder.add_from_resource ("/garden/turtle/Jellybean/help-overlay.ui");
                Gtk.ShortcutsWindow help_overlay = (Gtk.ShortcutsWindow) ui_builder.get_object ("help_overlay");
                help_overlay.view_name = "mv";                   // Set the main view as shown in the help overlay
                this.set_help_overlay (help_overlay);            // Set the help overlay
                nv.bind_property ("visible-page", help_overlay, "view-name", DEFAULT, this.on_prefs_bind);  // Bind the current navigation page to show its related shortcuts
            } catch (Error e) {
                critical ("Could not load help overlay: " + e.message);
            }

            // Devel CSS class
            if (Config.PROFILE == "Development") this.add_css_class ("devel");
        }

        // Binding function for shortcuts window — get tag name from navigation view and set that as the value
        private bool on_prefs_bind (Binding binding, Value val, ref Value to_val) {
            to_val = Value (typeof (string));
            to_val.set_string (nv.get_visible_page ().tag);
            return true;
        }

        // Saves changes in edit view
        private void save_jellybean () {
            if (nv.get_visible_page ().tag != "ev") return;

            // Save the current values to a jellybean
            Jellybean.Jellybeans jellybean = Jellybean.Jellybeans.from_props (
                ev_title.get_text (),
                nmadjustment.get_value (),
                ev_unit.get_text (),
                lsadjustment.get_value (),
                quadjustment.get_value (),
                int.parse (ev_icon_button.get_name ())
            );

            Jellybean.current_jellybean = jellybean;    // New jellybean → currently shown jellybean
            util.add_item (jellybean_index, jellybean); // Replace jellybean (or add new)
            util.jellybean_update ();                   // Recompile jellybeans
            if (jellybean_index == -1)
                util.update_rows (mv_lb);                      // Update rows
            else
                util.update_row_at_y (mv_lb, jellybean_index); // or just update one row

            // If the jellybean is new, don't do anything more. Return to main view.
            if (jellybean_index == -1) {
                Adw.Toast toast = new Adw.Toast (_("Item saved as %s").printf (jellybean.name));
                toast.timeout = 5;
                to.add_toast (toast);
                is_save = true;
                nv.pop_to_tag ("mv");
                return;
            }

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators: Low stock: is used in both main and item views
                infostr = _("Low stock: ") + jellybean.name;
            else
                infostr = jellybean.name;

            this.title_label.set_label (infostr);
            status_icon.icon_name = ICON_NAMES[(int) jellybean.icon];

            this.description_label.set_label (
                // Translators: @d %s → for example, 23 units or 495 units. Make sure this works when %s is blank. Double spaces are fine
                _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ()).replace("  ", " ")
            );

            this.sp1_sp_drain.set_sensitive (jellybean.number != 0);    // Deactivate use button if cannot use

            is_save = true;             // Item is saved
            is_new_jellybean = false;   // and not new

            Adw.Toast toast = new Adw.Toast (_("Item updated as %s").printf (jellybean.name));
            toast.timeout = 3;
            to.add_toast (toast);

            nv.pop_to_tag ("iv");
            return;
        }

        // Handles leaving the edit view. Is not actually a dialog
        private void discard_dialog () {
            if (!is_save && is_ev_shown) {                                      // Is the edit view shown? If so do most of this
                Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;   // Get the current jellybean
                bool was_new_jellybean = is_new_jellybean;                      // Was it a new jellybean?
                bool is_correct = is_new_jellybean ?                            // Ensure that new jellybeans get toast, and unchanged ones don't
                                  false :
                               (((ev_title.get_text () == jellybean.name) &&
                                 (nmadjustment.get_value () == jellybean.number)) &&
                                ((ev_unit.get_text () == jellybean.unit) &&
                                 (lsadjustment.get_value () == jellybean.low)) &&
                                ((quadjustment.get_value () == jellybean.quick) &&
                                 (int.parse (ev_icon_button.get_name ()) == jellybean.icon)));

                if (!is_correct) {
                    Adw.Toast toast = new Adw.Toast (!is_new_jellybean ?
          _("Changes to %s discarded").printf (jellybean.name) :
          _("Changes to item discarded"));
                    toast.set_button_label (_("Undo"));
                    toast.button_clicked.connect (() => {
                        nv.push_by_tag ("ev");                                      // Go back to edit view
                        if (was_new_jellybean) is_new_jellybean = true;             // If was new jellybean, make sure app knows it is new still
                    });
                    toast.timeout = 6;
                    to.add_toast (toast);
                }
                is_new_jellybean = false;                                       // Not a new jellybean anymore
            }
            is_ev_shown = false;     // Disable edit view finally
        }

        // Opens item view for a jellybean: bound to a row
        private void show_jellybean (string jellybeans_id, Jellybeans jellybean, bool undo = false) { // bool undo is not used, it keeps consistency with func type
            uint j = uint.parse (jellybeans_id ?? "0"); // Let's error gracefully

            ev_db.set_visible (util.jellybeans_object ().length != 1);

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators: Low stock: is used in both main and item views
                infostr = _("Low stock: ") + jellybean.name;
            else
                infostr = jellybean.name;

            this.title_label.set_label (infostr);
            status_icon.icon_name = ICON_NAMES[(int) jellybean.icon];

            this.description_label.set_label (
                // Translators: @d %s → for example, 23 units or 495 units. Make sure this works when %s is blank. Double spaces are fine
                _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ()).replace("  ", " ")
            );

            jellybean_index = (int) j;                  // let's show our current jellybean index
            Jellybean.current_jellybean = jellybean;    // and set our shown jellybean

            this.sp1_sp_drain.set_sensitive (jellybean.number != 0);    // Disable use button if there's nothing to use
            ev_db.set_visible (util.jellybeans_object ().length != 1);  // and disable delete button if this is the last jellybean

            // Legacy code
            if (is_new_jellybean && !is_save) {
                this.nv.pop_to_tag ("iv");
                return;
            }

            is_new_jellybean = false;
            this.nv.push_by_tag ("iv");
        }

        // "Uses" a selected item
        private void use_jellybean () {
            if (nv.get_visible_page ().tag != "iv") return;
            Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;

            /*
            Adw.MessageDialog msg = new Adw.MessageDialog (
                this,
                // Translators: %s is the jellybean's name
                _("Use %s").printf (jellybean.name),
                // Translators: %s of @s → units of Item
                _("How many %s of @s would you like to use?").printf (jellybean.unit).replace ("@s", jellybean.name)
            );
            msg.close_response = "cancel";
            msg.add_response ("cancel", _("Cancel"));
            // Translators: Imperative for using changes
            msg.add_response ("use", _("Use"));
            msg.set_response_appearance ("use", Adw.ResponseAppearance.SUGGESTED);

            double num;
            num = jellybean.number + 1;
            if (jellybean.number <= 1) num = jellybean.number;
            if (jellybean.number == 0) return;

            Gtk.Adjustment adj = new Gtk.Adjustment (jellybean.quick, 1, num, 1, 10, 1);
            Gtk.SpinButton spb = new Gtk.SpinButton (adj, 10, 0);
            msg.set_extra_child (spb);

            msg.response.connect ((response) => {
                if (response == "cancel") return;
                else { */


            if (jellybean.number < jellybean.quick) { // If there is not enough left, warn the user and do not follow through
                // Translators: %s → jellybean unit
                Adw.Toast toast = new Adw.Toast (_("Not enough %s left").printf (jellybean.unit));
                toast.timeout = 3;
                this.to.add_toast (toast);
                return;
            }

            jellybean.number -= jellybean.quick; // subtract amount
            // Adw.Toast toast = new Adw.Toast(""); // Must assign variable to not throw error


            // if (use_status != 2) { // If there is no undoing a refill, we should notify the user.
                // Translators: @d %s of @s → {number} units of Item
            //     toast = new Adw.Toast (_("Used @d %s of @s").printf (jellybean.unit).replace ("@d",
            //         jellybean.quick.to_string ()
            //     ).replace ("@s", jellybean.name));
            //     toast.set_priority (HIGH);
            //     toast.timeout = 3;
                // toast.set_button_label (_("Undo"));
                // toast.button_clicked.connect (() => {
                //     use_status = 1;
                //     this.refill_jellybean ();
                // });
            // }

            use_status = 0; // No undo operations any more

            this.util.add_item (jellybean_index, jellybean);    // Update the item
            this.util.update_row_at_y (mv_lb, jellybean_index); // and its row

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators: Low stock: is used in both main and item views
                infostr = _("Low stock: ") + jellybean.name;
            else
                infostr = jellybean.name;

            this.title_label.set_label (infostr);

            this.description_label.set_label (
                // Translators: @d %s → for example, 23 units or 495 units. Make sure this works when %s is blank. Double spaces are fine
                _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ()).replace("  ", " ")
            );

            status_icon.icon_name = ICON_NAMES[(int) jellybean.icon];
            if (jellybean.number == 0) sp1_sp_drain.set_sensitive (false);

            // if (use_status != 2) {
            //     Adw.Toast? deltoast = this.to.get_child () as Adw.Toast;
            //     if (deltoast != null) deltoast.dismiss ();
            //     if (toast.title != "") this.to.add_toast (toast);
            // }
            use_status = 0;
            return;

            /*
                }
            });

            msg.present ();
            */
        }

        // "Refills" a focused item
        private void refill_jellybean () {
            if (nv.get_visible_page ().tag != "iv") return;
            Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;

            /*
            Adw.MessageDialog msg = new Adw.MessageDialog (
                this,
                // Translators: %s is the jellybean's name
                _("Refill %s").printf (jellybean.name),
                // Translators: %s of @s → units of Item
                _("How many %s of @s would you like to add?").printf (jellybean.unit).replace ("@s", jellybean.name)
            );
            msg.close_response = "cancel";
            msg.add_response ("cancel", _("Cancel"));
            // Translators: Imperative for adding changes to jellybean
            msg.add_response ("add", _("Add"));
            msg.set_response_appearance ("add", Adw.ResponseAppearance.SUGGESTED);

            Gtk.Adjustment adj = new Gtk.Adjustment (jellybean.quick, 1, 2147483647 - jellybean.number, 1, 10, 1);
            Gtk.SpinButton spb = new Gtk.SpinButton (adj, 10, 0);
            msg.set_extra_child (spb);

            msg.response.connect ((response) => {
                if (response == "cancel") return;
                else { */
            jellybean.number += jellybean.quick;
            // Adw.Toast toast = new Adw.Toast(""); // Must assign variable to not throw error

            // if (use_status != 1) {
                // Translators: @d %s to @s → {number} units to Item
            //     toast = new Adw.Toast (_("Added @d %s to @s").printf (jellybean.unit).replace ("@d",
            //                  jellybean.quick.to_string ()
            //     ).replace ("@s", jellybean.name));
            //     toast.set_priority (HIGH);
            //     toast.timeout = 3;
                /// toast.set_button_label (_("Undo"));
                // toast.button_clicked.connect (() => {
                //     use_status = 2;
                //     this.use_jellybean ();
                // });
            // }

            this.util.add_item (jellybean_index, jellybean);    // Update the item
            this.util.update_row_at_y (mv_lb, jellybean_index); // and its row

            string infostr = "";
            if (jellybean.number <= jellybean.low)
                // Translators: Low stock: is used in both main and item views
                infostr = _("Low stock: ") + jellybean.name;
            else
                infostr = jellybean.name;

            this.title_label.set_label (infostr);

            this.description_label.set_label (
                // Translators: @d %s → for example, 23 units or 495 units. Make sure this works when %s is blank. Double spaces are fine
                _("@d %s left").printf (jellybean.unit).replace ("@d", jellybean.number.to_string ()).replace("  ", " ")
            );

            status_icon.icon_name = ICON_NAMES[(int) jellybean.icon];
            if ((sp1_sp_drain.get_sensitive () == false) && (jellybean.number > 0)) sp1_sp_drain.set_sensitive (true);

            // if (use_status != 1) {
            //     Adw.Toast? deltoast = this.to.get_child () as Adw.Toast;
            //     if (deltoast != null) deltoast.dismiss ();
            //     if (toast.title != "") this.to.add_toast (toast);
            // }
            use_status = 0;
            return;

            /*
                }
            });

            msg.present ();
            */
        }

        // Opens the New Item window
        private void add_jellybean () {
            if (nv.get_visible_page ().tag != "mv") return;
            ev_title.text = _("New Item");
            nmadjustment.set_value (50);
            ev_unit.text = "";
            lsadjustment.set_value (10);
            quadjustment.set_value (2);
            this.ev_icon_button.icon_name = ICON_NAMES[0];
            this.ev_icon_button.set_name ("0");
            ev.set_title (_("Editing %s").printf (_("New Item")));
            ev_db.set_visible (false);
            is_new_jellybean = true;
            is_save = false;

            nv.push_by_tag ("ev");
            jellybean_index = -1;
        }

        private void edit_jellybean (bool retur = true) {
            if (nv.get_visible_page ().tag != "iv") return;
            Jellybean.Jellybeans jellybean = Jellybean.current_jellybean;
            int icon = (int) jellybean.icon;
            ev_title.text = jellybean.name;
            nmadjustment.set_value (jellybean.number);
            ev_unit.text = jellybean.unit;
            lsadjustment.set_value (jellybean.low);
            quadjustment.set_value (jellybean.quick);
            ev_icon_button.icon_name = ICON_NAMES[icon];
            status_icon.icon_name = ICON_NAMES[icon];
            ev_icon_button.tooltip_text = ICON_TOOLTIPS[icon];
            ev_icon_button.set_name (icon.to_string ());
            iv.set_title (jellybean.name);
            ev.set_title (_("Editing %s").printf (jellybean.name));
            if (retur) nv.push_by_tag ("ev");
            is_save = false;
        }

        // Delete a jellybean
        private void delete_jellybean () {
            if (nv.get_visible_page ().tag == "mv") return; // If we're not looking at an item, things will break.
            if (ev_db.get_visible () == false) return;      // If the delete button is disabled, we should not allow the shortcut to work.

            // Create dialog to confirm deletion
            Adw.MessageDialog msg = new Adw.MessageDialog (
                this,
                _("Delete Item?"),
                _("If you delete this item, its information will be deleted permanently.")
            );
            msg.close_response = "cancel";

            msg.add_response ("cancel", _("_Cancel"));
            // Translators: Imperative for discarding changes
            msg.add_response ("discard", _("_Delete"));
            msg.set_response_appearance ("discard", Adw.ResponseAppearance.DESTRUCTIVE);

            msg.response.connect ((response) => {
                if (response == "cancel") return; // Do nothing if action canceled
                else {
                    util.delete_item (jellybean_index);                     // Delete selected item
                    util.update_rows (mv_lb);                               // Full update of rows
                    is_save = true;                                         // We have confirmed changes…
                    nv.pop_to_tag ("mv");                                   // …so we can leave the page…
                    Adw.Toast toast = new Adw.Toast (_("Item deleted"));    // …and notify the user that it was successfully deleted
                    toast.timeout = 3;
                    to.add_toast (toast);
                    return;
                }
            });

            msg.present ();
        }

        private void populate_icon_popover () {
            Jellybean.IconButton? icon_button = new Jellybean.IconButton (0); // Make a template icon button to start the function.
            Gtk.Box[] box = new Gtk.Box[2];                                   // Two rows in the popover
            int jenum = 0;  // Iterator of boxes
            int ienum;      // Iterator of icons
            do {
                box[jenum] = new Gtk.Box (HORIZONTAL, 6); // Create a box for the icons
                ienum = 3 * jenum;                        // 3 icons per row, thus we should set the index to thrice the row.
                do {
                    icon_button.clicked.connect ((button) => {              // On click, we should…
                        int icon = int.parse (button.get_name ());          // …get the icon number
                        ev_icon_button.icon_name = ICON_NAMES[icon];        // …set the main button icon to this button's icon
                        ev_icon_button.set_name (icon.to_string ());        // …set the icon's name to aid with saving item
                        ev_icon_button.tooltip_text = ICON_TOOLTIPS[icon];  // …set the tooltip
                        ev_icon_button.get_popover ().popdown ();           // …close the popover
                    });
                    box[jenum].append (icon_button);                // Add the icon button
                    ienum++;                                        // Increase the count of the icons
                    icon_button = new Jellybean.IconButton (ienum); // Create a new icon button
                } while (ienum <= (3 * jenum) + 2);

                popover_box.append (box[jenum]); // Add our populated box
                jenum++;                         // Move to the next one
            } while (jenum <= 1);
        }

        // Filter rows for search
        private bool filter_row (Gtk.ListBoxRow row) {
            Jellybean.Row new_row = (Jellybean.Row) row; // Coerce type to access icon

            // If there's no regex, the search entry is not open,
            // thus all rows should match.
            if (regex == null) return true;

            bool match = regex.match (new_row.title);    // Match within the title.
            if (!match) match = string_search_terms.match_with_regex (new_row.icon_index, regex);   // If that fails, match within the icon.
            if (match) is_showing_results = true;        // If either matched, results should be shown.

            return match;
        }
    }
}
