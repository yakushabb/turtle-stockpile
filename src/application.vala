/* application.vala
 *
 * Copyright 2023 skøldis
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

namespace Jellybean {
    GLib.Settings settings;
    public class Application : Adw.Application {
        public Application () {
            Object (application_id: Config.APP_ID, flags: ApplicationFlags.DEFAULT_FLAGS);
        }

        private string ar_flatpak;

        string felsokning = "OS: %s %s\nVersion: %s\nGTK: %u.%u.%u (%d.%d.%d)\nlibadwaita: %u.%u.%u (%d.%d.%d)\n\n%s\nProfile: %s\n\nLanguage: %s".printf ( // vala-lint = line-length
            Environment.get_os_info ("NAME"), Environment.get_os_info ("VERSION"),
            Config.VERSION,
            Gtk.get_major_version (), Gtk.get_minor_version (), Gtk.get_micro_version (), // Run time
            Gtk.MAJOR_VERSION, Gtk.MINOR_VERSION, Gtk.MICRO_VERSION,                      // Compile time
            Adw.get_major_version (), Adw.get_minor_version (), Adw.get_micro_version (), // Run time
            Adw.MAJOR_VERSION, Adw.MINOR_VERSION, Adw.MICRO_VERSION,                      // Compile time
            "%s", Config.PROFILE,
            Environment.get_variable ("LANG")
        );

        construct {
            Intl.setlocale (LocaleCategory.ALL, "");
            Intl.bindtextdomain (Config.GETTEXT_PACKAGE, Config.LOCALEDIR);
            Intl.bind_textdomain_codeset (Config.GETTEXT_PACKAGE, "UTF-8");
            Intl.textdomain (Config.GETTEXT_PACKAGE);

            settings = new GLib.Settings ("garden.turtle.Jellybean");

            ActionEntry[] action_entries = {
                { "about", this.om_aktion },
                { "quit", this.quit }
            };
            this.add_action_entries (action_entries, this);
            this.set_accels_for_action ("app.quit", { "<primary>q" }); // ensure styles load

            ar_flatpak = Environment.get_variable ("FLATPAK_ID") != null ?
                "Running in flatpak environment\nApp ID: %s".printf (Environment.get_variable ("FLATPAK_ID")) :
                "Running in unsupported environment";

            felsokning = felsokning.printf (ar_flatpak);
        }

        public override void activate () {
            base.activate ();
            var win = this.active_window;
            if (win == null) win = new Jellybean.Window (this);
            win.present ();
            settings.delay ();
        }

        private void om_aktion () {
            var om = new Adw.AboutWindow () {
                transient_for = this.active_window,
                application_name = _("Stockpile"),
                application_icon = Config.APP_ID,
                developer_name = "skøldis",
                version = Config.VERSION,
                developers = {
                    "skøldis <stockpile@turtle.garden>"
                },
                artists = {
                    "Brage Fuglseth https://bragefuglseth.dev"
                },
                copyright = "© 2023 skøldis",
                // Translators: do one of the following, one per line: Your Name, Your Name <email@email.org>, Your Name https://websi.te
                translator_credits = _("translator-credits"),
                license_type = Gtk.License.AGPL_3_0,
                issue_url = "https://codeberg.org/turtle/stockpile/issues/new",
                debug_info = felsokning,
                debug_info_filename = @"$(Config.APP_ID).txt"
            };

            om.add_credit_section (_("Contributors"), {
                // Contributors: do one of the following, one per line: Your Name, Your Name <email@email.org>, Your Name https://websi.te
            });

            om.add_credit_section (_("Other Helpers"), {
                "Felipe Kinoshita https://felipekinoshita.com/",
                "GNOME App Dev Hangout https://matrix.to/#/%23app-dev:gnome.org"
            });

            om.present ();
        }

        protected override void shutdown () {
            settings.apply ();
            base.shutdown ();
        }
    }
}
